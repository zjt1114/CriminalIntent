package com.zjt.criminalintent;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import android.content.Context;
import android.util.Log;

public class CrimeLab {

	private static final String FILENAME = "crimes.json";

	private final String TAG = this.getClass().getName();
	private List<Crime> crimes;
	private CriminalIntentJSONSerializer serializer;

	private static CrimeLab crimeLab;
	private Context appContext;

	private CrimeLab(Context appContext) {
		this.appContext = appContext;
		this.serializer = new CriminalIntentJSONSerializer(this.appContext,
				FILENAME);

		try {
			crimes = serializer.loadCrimes();
		} catch (Exception e) {
			crimes = new ArrayList<Crime>();
			Log.e(TAG, "Error loading crimes: ", e);
			Log.i(TAG, "Initializing a new list of crimes");
		}
	}

	public static CrimeLab get(Context c) {
		if (crimeLab == null) {
			crimeLab = new CrimeLab(c.getApplicationContext());
		}
		return crimeLab;
	}

	public void addCrime(Crime crime) {
		crimes.add(crime);
	}

	public void deleteCrime(Crime crime) {
		crimes.remove(crime);
	}

	public List<Crime> getCrimes() {
		return crimes;
	}

	public Crime getCrime(UUID id) {
		for (Crime c : crimes) {
			if (c.getId().equals(id))
				return c;
		}
		return null;
	}

	public boolean saveCrimes() {
		try {
			serializer.saveCrimes(crimes);
			Log.d(TAG, "crimes saved to file");
			return true;
		} catch (Exception e) {
			Log.e(TAG, "Error saving crimes: ", e);
			return false;
		}
	}

}