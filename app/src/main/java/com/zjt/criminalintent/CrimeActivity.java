package com.zjt.criminalintent;

import java.util.UUID;

import android.support.v4.app.Fragment;

public class CrimeActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return CrimeFragment.newInstance((UUID) getIntent()
				.getSerializableExtra(CrimeFragment.EXTRA_CRIME_ID));
	}
}
